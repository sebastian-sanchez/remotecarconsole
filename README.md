Carrito control remoto por consola 
==================================

Para ejecutar este programa es necesario abrirlo desde una IDE (Eclipse , Netbeans , Intellij idea) seleccionar la clase "App.java" y enviarla a ejecucion

Ejemplos :
--------------------

![Ejemplo de ejecucion](https://bitbucket.org/repo/eqp9kx/images/3425313841-Captura%20de%20pantalla%202016-11-04%20a%20las%209.34.42%20a.m..png)

Al iniciar el programa solicitará los parámetros de configuración del lienzo :

![Solicitud inicial de datos](https://bitbucket.org/repo/eqp9kx/images/3506703685-Captura%20de%20pantalla%202016-11-04%20a%20las%209.41.15%20a.m..png)

Mostrará en lienzo en sus versión inicial :

![Lienzo primera ves](https://bitbucket.org/repo/eqp9kx/images/849238968-Captura%20de%20pantalla%202016-11-04%20a%20las%209.41.23%20a.m..png)

Solicitará el ingreso de los movimientos y los mostrar en pantalla:

![Captura de pantalla 2016-11-04 a las 9.42.12 a.m..png](https://bitbucket.org/repo/eqp9kx/images/237868773-Captura%20de%20pantalla%202016-11-04%20a%20las%209.42.12%20a.m..png)

Para salir del programa escribir exit:

![Captura de pantalla 2016-11-04 a las 9.42.24 a.m..png](https://bitbucket.org/repo/eqp9kx/images/751288302-Captura%20de%20pantalla%202016-11-04%20a%20las%209.42.24%20a.m..png)