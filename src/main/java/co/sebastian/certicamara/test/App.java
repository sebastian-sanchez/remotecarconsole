package co.sebastian.certicamara.test;

import co.sebastian.certicamara.test.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.constans.Address;
import co.sebastian.certicamara.test.core.Car;
import co.sebastian.certicamara.test.core.Dashboard;
import co.sebastian.certicamara.test.validations.UtilValidate;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Created by ssanchez on 31/10/16.
 */
public class App {

    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int sizeDashboardY;
        int sizeDashboardX;
        Car car = new Car();

        while (true) {
            try {
                System.out.print("Ingrese la altura del lienzo : ");
                String input = scanner.nextLine();

                UtilValidate.getInstance().validateNumberFormat(input);

                sizeDashboardY = Integer.valueOf(input);

                System.out.print("Ingrese la ancho del lienzo : ");
                input = scanner.nextLine();

                UtilValidate.getInstance().validateNumberFormat(input);

                sizeDashboardX = Integer.valueOf(input);

                if (sizeDashboardX != 0 && sizeDashboardY != 0)
                    break;
            } catch (BussinesExcepcion be) {
                System.out.println(be.getMessage());
            }
        }


        Dashboard dashboard = new Dashboard(sizeDashboardX, sizeDashboardY, car);

        try {
            dashboard.show();
        } catch (BussinesExcepcion bussinesExcepcion) {
            bussinesExcepcion.printStackTrace();
        }

        while (true) {

            System.out.print("Ingrese el siguiente moviento : ");

            String movs = scanner.nextLine();
            if (movs.contains("exit")) {
                break;
            }
            try {
               UtilValidate.getInstance().validateFormatInput(movs);

                Map<Address, Integer> listMov = new HashMap<>();
                StringTokenizer st = new StringTokenizer(movs.toUpperCase(), ";");
                while (st.hasMoreTokens()) {
                    String[] mov = st.nextToken().split(",");

                    UtilValidate.getInstance().validateNumberFormat(mov[1]);

                    listMov.put(Address.fromString(mov[0]), Integer.valueOf(mov[1]));
                }

                for (Map.Entry<Address, Integer> entry : listMov.entrySet()) {
                    Address addres = entry.getKey();
                    Integer sizeMove = entry.getValue();
                    car.moveCar(addres, sizeMove);
                    //dashboard.show();
                }

                dashboard.show();

            } catch (BussinesExcepcion be) {
                System.out.println(be.getMessage());
            }
        }

        scanner.close();
    }
}
