import co.sebastian.certicamara.test.constans.Address;
import co.sebastian.certicamara.test.core.Car;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ssanchez on 2/11/16.
 */
public class CarTest {

    @Test
    public void testValidateInitPosicionCar() {
        //DADO

        Car car;

        //CUANDO
        car = new Car();

        //ESPERO

        assertEquals(car.getCurrentPositionY(), 0);
        assertEquals(car.getCurrentPositionX(), 0);

    }

    @Test
    public void testValidateInitPosicionMov1() {
        //DADO

        Car car = new Car();
        Address address = Address.NORTH;
        int sizemov = 3;

        //CUANDO
        car.moveCar(address,sizemov);

        //ESPERO

        assertEquals(car.getCurrentPositionY(), 3);
        assertEquals(car.getCurrentPositionX(), 0);

    }

    @Test
    public void testValidateInitPosicionMov2() {
        //DADO

        Car car = new Car();
        Address address1 = Address.NORTH;
        int sizemov1 = 3;

        Address address2 = Address.WEST;
        int sizemov2 = 2;

        //CUANDO
        car.moveCar(address1,sizemov1);
        car.moveCar(address2,sizemov2);

        //ESPERO

        assertEquals(car.getCurrentPositionY(), 3);
        assertEquals(car.getCurrentPositionX(), 2);

    }


    @Test
    public void testValidateInitPosicionMov3() {
        //DADO

        Car car = new Car();
        Address address1 = Address.WEST;
        int sizemov1 = 3;

        Address address2 = Address.EAST;
        int sizemov2 = 2;

        //CUANDO
        car.moveCar(address1,sizemov1);
        car.moveCar(address2,sizemov2);

        //ESPERO

        assertEquals(car.getCurrentPositionY(), 0);
        assertEquals(car.getCurrentPositionX(), 1);

    }

    @Test
    public void testValidateInitPosicionReset() {
        //DADO

        Car car = new Car();
        Address address1 = Address.NORTH;
        int sizemov1 = 3;

        Address address2 = Address.SOUTH;
        int sizemov2 = 2;

        //CUANDO
        car.moveCar(address1,sizemov1);
        car.moveCar(address2,sizemov2);
        car.revertMoveCar();

        //ESPERO

        assertEquals(car.getCurrentPositionY(), 3);
        assertEquals(car.getCurrentPositionX(), 0);

    }
}
