import co.sebastian.certicamara.test.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.constans.Address;
import co.sebastian.certicamara.test.core.Car;
import co.sebastian.certicamara.test.core.Dashboard;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by ssanchez on 2/11/16.
 */
public class DashboardTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

    @Test
    public void testCreateDashboarSucces() throws BussinesExcepcion {

        //DADO

        int sizeY = 12;
        int sizeX = 12;
        Car car = new Car();

        //ENTONCES

        Dashboard dashboard = new Dashboard(sizeX, sizeY, car);
        dashboard.show();

        //ESPERO

        assertEquals("_____________________________________________________\n" +
                "Pocicion en x : 0\n" +
                "Pocicion en Y : 0\n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " *  _  _  _  _  _  _  _  _  _  _  _ \n", outContent.toString());

    }

    @Test(expected = BussinesExcepcion.class)
    public void testValidateLimitSucces() throws BussinesExcepcion {

        //DADO

        int sizeY = 12;
        int sizeX = 12;
        Car car = new Car();

        //ENTONCES

        Dashboard dashboard = new Dashboard(sizeX, sizeY, car);
        car.moveCar(Address.NORTH, 15);
        dashboard.show();

        //ESPERO

        fail("Esperaba una excepcion");
    }

    @Test
    public void testOutPosicionSucces() throws BussinesExcepcion {

        //DADO

        int sizeY = 12;
        int sizeX = 12;
        Car car = new Car();

        //ENTONCES

        Dashboard dashboard = new Dashboard(sizeX, sizeY, car);
        car.moveCar(Address.NORTH, 5);
        dashboard.show();

        //ESPERO

        assertEquals("_____________________________________________________\n" +
                "Pocicion en x : 0\n" +
                "Pocicion en Y : 5\n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " *  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n" +
                " _  _  _  _  _  _  _  _  _  _  _  _ \n", outContent.toString());
    }


}
