import co.sebastian.certicamara.test.Excepcion.BussinesExcepcion;
import co.sebastian.certicamara.test.validations.UtilValidate;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by ssanchez on 2/11/16.
 */
public class UtilValidateTest {

    @Test(expected = BussinesExcepcion.class)
    public void testValidationFormInputError1() throws BussinesExcepcion {
        //DADO
        String input = "123456";

        //CUANDO


        UtilValidate.getInstance().validateFormatInput(input);


        //ESPERO

        fail( "Esperaba una excepcion" );
    }

    @Test(expected = BussinesExcepcion.class)
    public void testValidationFormInputError2() throws BussinesExcepcion {
        //DADO
        String input = "Q,N";

        //CUANDO


        UtilValidate.getInstance().validateFormatInput(input);


        //ESPERO

        fail( "Esperaba una excepcion" );
    }

    @Test(expected = BussinesExcepcion.class)
    public void testValidationFormInputError3() throws BussinesExcepcion {
        //DADO
        String input = "N,3;12.2";

        //CUANDO


        UtilValidate.getInstance().validateFormatInput(input);


        //ESPERO

        fail( "Esperaba una excepcion" );
    }

    @Test
    public void testValidationFormInputSucces1() throws BussinesExcepcion {
        //DADO
        String input = "N,2";

        //CUANDO


        UtilValidate.getInstance().validateFormatInput(input);


        //ESPERO

        assertTrue(true);
    }

    @Test
    public void testValidationFormInputSucces2() throws BussinesExcepcion {
        //DADO
        String input = "N,2;S,4";

        //CUANDO


        UtilValidate.getInstance().validateFormatInput(input);


        //ESPERO

        assertTrue(true);
    }

    @Test
    public void testValidationFormInputSucces3() throws BussinesExcepcion {
        //DADO
        String input = "N,2;S,2;O,1;E,3";

        //CUANDO


        UtilValidate.getInstance().validateFormatInput(input);


        //ESPERO

        assertTrue(true);
    }


    @Test(expected = BussinesExcepcion.class)
    public void testvalidateNumberFormatError1() throws BussinesExcepcion {
        //DADO
        String input = "N,";

        //CUANDO


        UtilValidate.getInstance().validateNumberFormat(input);


        //ESPERO

        fail( "Esperaba una excepcion" );
    }

    @Test
    public void testValidateNumberFormatSucces1() throws BussinesExcepcion {
        //DADO
        String input = "10";

        //CUANDO


        UtilValidate.getInstance().validateNumberFormat(input);


        //ESPERO

        assertTrue(true);
    }
}
