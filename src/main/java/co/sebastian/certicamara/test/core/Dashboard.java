package co.sebastian.certicamara.test.core;

import co.sebastian.certicamara.test.Excepcion.BussinesExcepcion;

/**
 * Created by ssanchez on 31/10/16.
 */
public class Dashboard {

    private int sizeX;
    private int sizeY;

    private Car car;

    public Dashboard(int sizeX, int sizeY, Car car) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.car = car;
    }

    public void show() throws BussinesExcepcion {

        validateLimitsSize();
        showNextPosicion();

        for (int i = sizeY-1; i >= 0; i--) {
            StringBuilder row = new StringBuilder();
            for (int j = 0; j < sizeX; j++) {
                if (j == car.getCurrentPositionX() && i == car.getCurrentPositionY()) {
                    row.append(" * ");
                } else {
                    row.append(" _ ");
                }
            }
            System.out.println(row);
        }
    }

    private void validateLimitsSize() throws BussinesExcepcion {
        if (sizeX < car.getCurrentPositionX() || 0 > car.getCurrentPositionX() || sizeY < car.getCurrentPositionY() || 0 > car.getCurrentPositionY()) {
            car.revertMoveCar();
            throw new BussinesExcepcion("Se ha detenido el avance por salir de los límites");
        }
    }

    private void showNextPosicion() {
        System.out.println("_____________________________________________________");
        System.out.println("Pocicion en x : " + car.getCurrentPositionX());
        System.out.println("Pocicion en Y : " + car.getCurrentPositionY());
    }

}
