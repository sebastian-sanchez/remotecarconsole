package co.sebastian.certicamara.test.constans;

/**
 * Created by ssanchez on 1/11/16.
 */
public enum Address {

    NORTH("N"),
    WEST("O"),
    EAST("E"),
    SOUTH("S");

    private String symbol;

    Address(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public static Address fromString(String symbol) {
        for (Address address : Address.values()) {
            if (symbol.equals(address.getSymbol())) {
                return address;
            }
        }
        return null;
    }
}
