import co.sebastian.certicamara.test.constans.Address;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ssanchez on 2/11/16.
 */
public class AddressTest {

    @Test
    public void testStringToEnumAddresError() {
        //DADO

        String innput = "QAZ";

        //ENTONCES

        Address address = Address.fromString(innput);

        //ESPERO

        assertEquals(null, address);
    }

    @Test
    public void testStringToEnumAddresSucces() {
        //DADO

        String innput = "N";

        //ENTONCES

        Address address = Address.fromString(innput);

        //ESPERO

        assertEquals(Address.NORTH, address);
    }
}
