package co.sebastian.certicamara.test.core;

import co.sebastian.certicamara.test.constans.Address;

/**
 * Created by ssanchez on 1/11/16.
 */
public class Car {

    private int currentPositionX;
    private int currentPositionY;

    private int oldPositionX;
    private int oldPositionY;


    public Car() {
        beginPosition();
    }

    private void beginPosition() {
        this.currentPositionX = 0;
        this.currentPositionY = 0;
        this.oldPositionX = 0;
        this.oldPositionY = 0;
    }

    public void moveCar(Address address, int sizeMove) {

        setOldPosition();
        switch (address) {
            case NORTH:
                this.currentPositionY = this.currentPositionY + sizeMove;
                break;
            case SOUTH:
                this.currentPositionY = this.currentPositionY - sizeMove;
                break;
            case WEST:
                this.currentPositionX = this.currentPositionX + sizeMove;
                break;
            case EAST:
                this.currentPositionX = this.currentPositionX - sizeMove;
                break;
        }

    }

    public void revertMoveCar() {
        this.currentPositionX = this.oldPositionX;
        this.currentPositionY = this.oldPositionY;
        this.oldPositionX = 0;
        this.oldPositionY = 0;
    }

    private void setOldPosition() {
        this.oldPositionX = this.getCurrentPositionX();
        this.oldPositionY = this.getCurrentPositionY();

    }

    public int getCurrentPositionX() {
        return currentPositionX;
    }

    public int getCurrentPositionY() {
        return currentPositionY;
    }


}
